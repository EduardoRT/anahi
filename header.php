<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name= "viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=<?php echo THEME_VERSION; ?>">
  <title><?php bloginfo('name'); wp_title('|'); ?></title>
  <?php wp_head(); ?>
</head>

<div class="container">
  <div class="navigation">
    <a href="<?php echo get_site_url(); ?>">
      <img class="logo" src="<?php echo get_template_directory_uri() . '/dist/images/logo.jpg'; ?>" alt="<?php bloginfo('name'); ?>">
    </a>

    <div class="links">
      <?php wp_nav_menu(array(
        'menu' => 'primary'
      )); ?>
    </div>
  </div>
</div>

<body <?php body_class(); ?>>
