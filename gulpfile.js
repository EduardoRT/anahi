var gulp         = require('gulp');
var sass         = require('gulp-sass');
var concat       = require('gulp-concat');
var cssnano      = require('gulp-cssnano');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var uglify       = require('gulp-uglify');
var imagemin     = require('gulp-imagemin');

var assets  = 'assets/',
    js_dir  = assets + 'js/',
    css_dir = assets + 'stylesheets/',
    img_dir = assets + 'images/',
    svg_dir = assets + 'images/svgs/';

var dist       = 'dist/',
    js_dist    = dist + 'js/',
    css_dist   = dist + 'stylesheets/'
    img_dist   = dist + 'images/',
    bower_dist = dist + 'bower_components/',
    svg_dist   = dist + 'images/';

var concatOrder = [
  "node_modules/normalize.css/normalize.css",
  "assets/stylesheets/application.scss",
  "assets/stylesheets/support/before/*.scss",
  "assets/stylesheets/support/*.scss",
  "assets/stylesheets/shared/*.scss",
  "assets/stylesheets/objects/*.scss",
  "assets/stylesheets/views/*.scss",
  "assets/stylesheets/support/after/*.scss",
]

var projectName = __dirname.split('/').pop();

var sassConfig = {
  outputStyle: 'compressed',
};

var cssnanoOptions = {
  zindex        : false,
  discardUnused : false,
  options: {
    autoprefixer      : false,
    safe              : true,
    discardDuplicates : false,
    sourcemap         : false
  }
};

gulp.task('default', ['build:css', 'build:js', 'optimize']);

gulp.task('build:css', function() {
  return gulp.src( concatOrder )
    .pipe(concat('application.css'))
    .pipe(sass(sassConfig).on('error', sass.logError))
    .pipe(postcss([autoprefixer()]))
    .pipe(cssnano(cssnanoOptions))
    .pipe(gulp.dest( css_dist ))
    .on('error', function(err) {
      console.error('Error!', err.message);
    });
});

gulp.task('build:js', function() {
  return gulp.src( js_dir + '**/*.js' )
     .pipe(uglify())
     .pipe(gulp.dest( js_dist ))
     .on('error', function(err) {
       console.error('Error!', err.message);
     });
   });

 gulp.task('optimize', function() {
   return gulp.src( img_dir + '**/*' )
     .pipe(imagemin())
     .pipe(gulp.dest( img_dist ))
 });
