<?php
get_header();
the_post();
?>

<div class="container">
<div class="content">
  <div class="display">
    <?php the_post_thumbnail(get_the_ID()); ?>
  </div>
  <?php the_content(); ?>
  </div>
</div>

<?php get_footer(); ?>
