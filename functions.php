<?php
define('THEME_VERSION', "1.2");

add_image_size('slider', '1000', '500', true);

add_action('wp_enqueue_scripts', function() {
  wp_enqueue_script( 'application.js', get_template_directory_uri() . '/dist/js/application.js', array('jquery'), THEME_VERSION );
  wp_enqueue_style( 'application.css', get_template_directory_uri() . '/dist/stylesheets/application.css', array(), THEME_VERSION );

  // Siema
  wp_enqueue_script( 'siema.js', get_template_directory_uri() . '/node_modules/siema/dist/siema.min.js', array(), THEME_VERSION );

  // Chocolat ( lightbox )
  wp_enqueue_script( 'chocolat.js', get_template_directory_uri() . '/node_modules/chocolat/dist/js/jquery.chocolat.min.js', array('jquery'), THEME_VERSION );
  wp_enqueue_style( 'chocolat.css', get_template_directory_uri() . '/node_modules/chocolat/dist/css/chocolat.css', array(), THEME_VERSION );
});

add_action('after_setup_theme', function() {
  add_theme_support('post-formats');
  add_theme_support('automatic-feed-links');
  add_theme_support('html5');
  add_theme_support('post-thumbnails');

  register_nav_menu(
    'primary',
    __('Primary Navigation', 'Menu Home')
  );
});

// add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}

// tag hooks
add_action('init', 'tags_support_all');

add_shortcode('contacto', function($atts) {
  if ( isset( $_POST['sender'] ) && isset( $_POST['email'] ) ) {
    $admin_email = get_option('admin_email');
    $to          = $admin_email;
    $subject     = 'Nuevo Contacto';
    $body        = sanitize_text_field($_POST['subject']);
    $headers[]   = sprintf('From: Anahí Echeverría <%s>', $admin_email);
    $header[]    = sprintf('Cc: %s <%s>', $_POST['sender'], $_POST['email']);
    $message     = sanitize_text_field($_POST['message']);
    $mail_sent   = wp_mail( $to, $subject, $message, $headers );
  }
  ob_start();

  if ( isset($mail_sent) ) {
    printf('<h1 style="text-align: center;">¡Gracias por comunicarte con nosotros!</h1>');
  }
?>

<div class="contact-form">
  <div class="display">
    <img src="http://nebula.wsimg.com/ebd0c1b7b14b19332fcdc0408c7ede44?AccessKeyId=D54B105A48E27E464F88&disposition=0&alloworigin=1" alt="">
  </div>
  <div class="form">
    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" name="contacto" method="POST">
      <label for="sender">Nombre:</label>
      <input id="sender" type="text" name="sender" required>

      <label for="email">Correo electrónico:</label>
      <input id="email" type="email" name="email">

      <label for="subject">Asunto:</label>
      <input id="subject" type="text" name="subject">

      <label for="message">Mensaje:</label>
      <textarea id="message" name="message" rows="8"></textarea>

      <div class="checkbox">
        <label for="newsletter">Suscribirse al newsletter</label>
        <input id="newsletter" type="checkbox" name="newsletter">
      </div>

      <input type="submit" value="Enviar">
    </form>
  </div>
</div>

<?php
  return ob_get_clean();
});
