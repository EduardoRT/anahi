<?php
get_header();
the_post();
?>
<div class="container">
  <div class="content">
    <?php the_content(); ?>
  </div>
</div>
<?php get_footer(); ?>
