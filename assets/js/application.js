jQuery(document).ready( function() {

  jQuery('.row').Chocolat({
    'imageSelector': '.gallery-anchor',
    'imageSize': 'native',
    'fullscreen': false,
    'loop': true,
  });

  jQuery('.slide').css('display', 'block');

  if ( window.location.pathname === '/' ) {
    var siema = new Siema({
      selector: '.slider',
      duration: 500,
      easing: 'ease-out',
      perPage: 1,
      startIndex: 0,
      draggable: true,
      threshold: 20,
      loop: true,
    });

    // listen for keydown event
    setInterval(function () {
      return siema.next();
    }, 3500);

    // keyboard navigation
    document.addEventListener('keydown', function (e) {
      if (e.keyCode === 37) {
        siema.prev();
      } else if (e.keyCode === 39) {
        siema.next();
      }
    });
  }
});

