<?php
/**
 * Template Name: Gallery
 * Template Post Type: page
 *
 */

$tags = get_the_tags() ? get_the_tags() : array();

$tags = array_map(function($tag) {
  return $tag->slug;
}, $tags);

$args = [
  'post_status'      => 'publish',
  'posts_per_page'   => -1,
  'offset'           => 0,
  'post_type'        => 'post',
  'orderby'          => 'date',
  'order'            => 'DESC',
  'tag'              => $tags,
  'suppress_filters' => true
];

$posts = get_posts($args);
$posts = array_chunk($posts, 5);
$count = 0;

get_header();
?>

<div class="container">
  <div class="gallery">
    <?php foreach($posts as $index => $post): ?>
      <div class="row <?php echo ($index % 2 == 0) ? "" : "odd"; ?>">
        <?php foreach($post as $image): ?>
        <a class="gallery-anchor" href="<?= get_the_post_thumbnail_url($image->ID); ?>">
          <?= get_the_post_thumbnail($image->ID, 'post-thumbnail', array('class' => 'image')); ?>
        </a>
        <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>

<?php get_footer(); ?>
